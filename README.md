# Ansible integration in Terraform 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create Ansible Playbook for Terraform integration 

* Adjust Terraform configuration to execute Ansible Playbook automatically, so once Terraform provisions server, it executes an Ansible playbook that configures the server 

## Technologies Used 

* Ansible 

* Terraform 

* AWS

* Docker 

* Linux 

## Steps 

Step 1: Add provisioner local exec in terraform configuration file, use working directory attribute to go into directory of ansible projects

[provisioner local exec in terraform config file](/images/01_add_provisioner_local_exec_in_terraform_configuration_file_use_working_dir_attribute_to_go_into_directory_of_ansible_projects.png)

Step 2: Use command attribute to write the command to execute the playbook

[Command attribute](/images/02_use_command_attribute_to_write_the_command_to_execute_the_playbook.png)

Step 3: In the command attribute ansible needs ip address of host that will be created by terraform resource  ec2 instance so what you need to do is use inventory flag self.public_ip this is because we are executing command in ec2 instance resource 

[inventory flag](/images/03_in_the_command_attribute_ansible_needs_ip_address_of_host_that_will_be_created_by_terraforms_resource_ec2_instance_so_what_we_need_to_do_is_use_inventory_flag_self_public_ip_this_is_because_we_are_executing_command_in_ec2_instance_resource.png)

Step 4: You need to add another flag for private key and reference variable which you will create

[--private-key](/images/04_we_need_to_add_another_flag_for_private_key_and_reference_variable_which_we_will_create.png)

Step 5: Insert variable in terraform config file 

[ssh key variable](/images/05_insert_variable_in_terraform_config_file.png)

Step 6: Save path of private key in the variable in the tfvars file 

[tfvars](/images/06_save_path_of_private_key_in_the_variable_in_the_tfvars_file.png)

Step 7: Add user flag as part of the command and give it name of user 

[user flag](/images/07_add_user_flag_as_part_of_the_command_and_give_it_name_of_user.png)

Step 8: Change every host value to all in playbook so it means whatever inventory is defined for this playbook you want to execute all of them 

[changing host value](/images/08_change_every_host_value_to_all_in_playbook_so_it_means_whatever_inventory_is_defined_for_this_play_book_we_want_to_execute_all_of_them.png)

Step 9: Add a new play to wait till port 22 is open before executing the other play 

[New play](/images/09_add_a_new_play_to_wait_for_ssh_connaction_give_host_set_gathering_facts_to_false_name_task_and_use_module_wait_for_to_wait_till_port_22_is_open_before_executing_the_other_plays.png)

Step 10: Test play

[Terraform apply success](/images/10_terraform_apply_success.png)
[docker success](/images/11_successful_docker.png)

Step 11: To make code cleaner create a different resource null resource for the ansible configuration change ip address to resource name instead because the command are not taken place in the ec2 resource anymore 

[Null resource](/images/12_to_make_code_cleaner_create_a_different_resource_null_resource_for_the_ansible_configuration_change_ip_address_to_resource_name_instead_because_the_commands_arent_taken_place_in_thec2_resource_anymore.png)

Step 12: Perform terraform inti 

    terraform init 

[Terraform init](/images/13_perform_terraform_init%20.png)

Step 13: Perform terraform apply 

    terraform apply

[Terraform apply](/images/14_successful_terraform_apply.png)

## Installation

    brew install ansible 
    brew install terraform 

## Usage 

    terraform apply 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-ansible-integration-in-terraform.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-ansible-integration-in-terraform

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.